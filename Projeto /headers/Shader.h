#ifndef SHADER_H
#define SHADER_H

#include "../include/glad/glad.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <cerrno>

using namespace std;

string get_file_contents(const char *filename);

class Shader
{
public:
    GLuint ID;
    Shader(const char *vertexFile, const char *fragmentFile);

    void Activate();
    void Delete();

private:
    // Checks if the different Shaders have compiled properly
    void compileErrors(unsigned int shader, const char *type);
};

#endif