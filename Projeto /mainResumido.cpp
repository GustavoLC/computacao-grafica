#include <glm/glm.hpp>
#include <glm/gtx/string_cast.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <GLFW/glfw3.h>
#include <iostream>

using namespace std;
using namespace glm;

int main(void)
{
    GLFWwindow *window;

    /* Initialize the library */
    if (!glfwInit())
        return -1;

    /* Create a windowed mode window and its OpenGL context */
    window = glfwCreateWindow(800, 800, "Hello World", NULL, NULL);
    if (!window)
    {
        glfwTerminate();
        return -1;
    }

    /* Make the window's context current */
    glfwMakeContextCurrent(window);

    vec4 bla1(1, 0, 0, 0);
    vec4 bla2(0, 1, 0, 0);
    vec4 bla3(0, 0, 1, 0);
    vec4 bla4(0, 0, 0, 1);

    mat4 myMatrix = translate(mat4(bla1, bla2, bla3, bla4), vec3(0.02, 0, 0));

    vec4 I(-0.5f, -0.5f, 0.0f, 1.0f);
    vec4 II(0.5f, -0.5f, 0.0f, 1.0f);
    vec4 III(0.0f, 0.5f, 0.0f, 1.0f);

    /* Loop until the user closes the window */
    while (!glfwWindowShouldClose(window))
    {
        glfwPollEvents();

        if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
        {
            glfwSetWindowShouldClose(window, GLFW_TRUE);
        }

        /* Render here */
        glClear(GL_COLOR_BUFFER_BIT);
        glColor3f(0.77, 0.01, 0.31);

        I = myMatrix * I;
        II = myMatrix * II;
        III = myMatrix * III;

        glBegin(GL_TRIANGLES);
        glVertex4f(I[0], I[1], I[2], I[3]);
        glVertex4f(II[0], II[1], II[2], II[3]);
        glVertex4f(III[0], III[1], III[2], III[3]);
        glEnd();

        glfwSwapBuffers(window);
    }

    glfwTerminate();
    return 0;
}
