# Computação Gráfica

## Uteis:
## Como compilar 
- g++ -pthread -o test main.cpp glad.c -lglfw -lGLU -lGL -lXrandr -lXxf86vm -lXi -lXinerama -lX11 -lrt -ldl
- g++ -pthread -o test mainResumido.cpp glad.c -lglfw -lGLU -lGL -lXrandr -lXxf86vm -lXi -lXinerama -lX11 -lrt -ldlD

## Como usar o Glad
- https://rpxomi.github.io/

##  Lib de Matrizes (tutorial)
- http://www.opengl-tutorial.org/beginners-tutorials/tutorial-3-matrices/

## Tutorial OpenGL em vídeo
- https://www.youtube.com/playlist?list=PLPaoO-vpZnumdcb4tZc4x5Q-v7CkrQ6M-